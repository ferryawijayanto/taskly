//
//  TaskStore.swift
//  Taskly
//
//  Created by Ferry Adi Wijayanto on 08/03/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class TaskStore {
    
    var items = [[Task](), [Task]()]
    
    // add Task
    func add(_ task: Task, at index: Int, isDone: Bool = false) {
        let section = isDone ? 1 : 0
        
        items[section].insert(task, at: index)
    }
    
    // remove Task
    @discardableResult func removeTask(at index: Int, isDone: Bool = false) -> Task {
        let section = isDone ? 1 : 0
        
        return items[section].remove(at: index)
    }
}
