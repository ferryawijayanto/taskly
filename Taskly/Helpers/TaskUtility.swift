//
//  TaskUtility.swift
//  Taskly
//
//  Created by Ferry Adi Wijayanto on 09/03/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class TaskUtility {
    private static let key = "tasks"
    
    // archive
    private static func archive(_ tasks: [[Task]]) -> Data? {
        
        return try? NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
    }
    
    // fetch
    static func fetch() -> [[Task]]? {
        guard let unarchiveData = UserDefaults.standard.object(forKey: key) as? Data else { return nil }
        
        return NSKeyedUnarchiver.unarchiveObject(with: unarchiveData) as? [[Task]]
    }
    
    // save
    static func save(_ tasks: [[Task]]) {
        // Archive data
        let archiveTask = archive(tasks)
        
        // Set object for key
        UserDefaults.standard.set(archiveTask, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
