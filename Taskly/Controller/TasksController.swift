//
//  TasksController.swift
//  Taskly
//
//  Created by Ferry Adi Wijayanto on 08/03/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class TasksController: UITableViewController {
    
    var taskStore: TaskStore! {
        didSet {
            // Fetch data
            taskStore.items = TaskUtility.fetch() ?? [[Task](), [Task]()]
            
            // Reload table view data
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeUnavailableTableViewData()
    }
}

// MARK: - Private extension
private extension TasksController {
    @IBAction func addTask(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Add Task", message: nil, preferredStyle: .alert)
        
        let addAction = UIAlertAction(title: "Add", style: .default) { _ in
            
            // Grab text field
            guard let text = alert.textFields?.first?.text else { return }
            
            // Create task
            let task = Task(name: text)
            
            // Add task
            self.taskStore.add(task, at: 0)
            
            // Insert new data in table view
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            
        }
        addAction.isEnabled = false
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        // add TextField in alert controller
        alert.addTextField { textField in
            textField.placeholder = "Enter task name..."
            textField.addTarget(self, action: #selector(self.handleTextChanged), for: .editingChanged)
        }
        
        alert.addAction(addAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    @objc private func handleTextChanged(_ sender: UITextField) {
        guard
            let alertController = presentedViewController as? UIAlertController,
            let action = alertController.actions.first,
            let task = sender.text
            else { return }
        
        // Enable add action if text is empty or contain whitespaces
        action.isEnabled = !task.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    private func removeUnavailableTableViewData() {
        tableView.tableFooterView = UIView()
    }
}

// MARK: - DataSource
extension TasksController {
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To-do" : "Done"
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return taskStore.items.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskStore.items[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let item = taskStore.items[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = item.name
        
        return cell
    }
}


// MARK: - Delegate
extension TasksController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (contextualAction, view, completionHandler) in
            
            let item = self.taskStore.items[indexPath.section][indexPath.row]
            
            // Check if the task 'isDone'
            guard let isDone = item.isDone else { return }
            
            // Remove the task in an array
            self.taskStore.removeTask(at: indexPath.row, isDone: isDone)
            
            // Delete task in table view
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            // Indicate that the action was performed
            completionHandler(true)
        }
        deleteAction.image = #imageLiteral(resourceName: "delete")
        deleteAction.backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.1450980392, blue: 0.168627451, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let doneAction = UIContextualAction(style: .normal, title: nil) { (contextualAction, view, completionHandler) in
            let item = self.taskStore.items[0][indexPath.row]
            
            // Remove task in an array
            self.taskStore.removeTask(at: indexPath.row).isDone = true
            
            // Delete task in task array
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            // Add task in an done array
            self.taskStore.add(item, at: 0, isDone: true)
            
            // Insert row in done task
            tableView.insertRows(at: [IndexPath(item: 0, section: 1)], with: .automatic)
            
            // Indicate that the action was performed
            completionHandler(true)
        }
        
        doneAction.image = #imageLiteral(resourceName: "done")
        doneAction.backgroundColor = #colorLiteral(red: 0.01176470588, green: 0.7529411765, blue: 0.2901960784, alpha: 1)
        
        // Make if the done action is not allow to swipe done again
        return indexPath.section == 0 ? UISwipeActionsConfiguration(actions: [doneAction]) : nil
    }
}
